<?php

namespace Drupal\random_quotes_and_facts_generator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure random quotes and facts settings.
 */
class RandomQuotesFactsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'random_quotes_facts_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['random_quotes_and_facts_generator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('random_quotes_and_facts_generator.settings');

    $form['api_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Host'),
      '#default_value' => 'https://api.api-ninjas.com/v1/',
      '#disabled' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Please provide API Ninjas,
      <a href="https://api-ninjas.com/register" target="_blank"><b>Get API Key</b></a>'),
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [
        'quotes' => $this->t('Quotes'),
        'Facts' => $this->t('Facts'),
      ],
      '#default_value' => $config->get('type') ?? 'quotes',
      '#required' => TRUE,
    ];

    $form['quote_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quote Type'),
      '#description' => $this->t('Please provide the Single Category for the quotes
      <a href="https://api-ninjas.com/api/quotes#:~:text=Parameters-,category,-(optional)%20%2D%20category%20to" target="_blank"><b>Click Here</b></a>'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'quotes'],
        ],
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('quote_type') ?? 'happiness',
    ];

    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('The number of Quotes or Facts to display on block.'),
      '#min' => 1,
      '#max' => 10,
      '#default_value' => $config->get('limit') ?? 1,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('random_quotes_and_facts_generator.settings');

    $config->set('api_host', $form_state->getValue('api_host'));
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('type', $form_state->getValue('type'));
    $config->set('quote_type', $form_state->getValue('quote_type'));
    $config->set('limit', $form_state->getValue('limit'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
