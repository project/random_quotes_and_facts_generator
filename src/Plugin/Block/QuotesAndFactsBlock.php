<?php

namespace Drupal\random_quotes_and_facts_generator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Random Quotes And Facts Generator' block.
 *
 * @Block(
 *   id = "random_quotes_and_facts_generator_block",
 *   admin_label = @Translation("Random Quotes And Facts Generator"),
 *   category = @Translation("Custom")
 * )
 */
class QuotesAndFactsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Instantiate the HTTP client.
    $clientFactory = \Drupal::service('http_client_factory');
    $client = $clientFactory->fromOptions([
      'verify' => TRUE,
    ]);
    $config = \Drupal::config('random_quotes_and_facts_generator.settings');
    // Set the API endpoint URL.
    $limit = $config->get('limit');
    $host = $config->get('api_host');
    $type = $config->get('type');
    $quote_type = $config->get('quote_type');
    if ($type === 'quotes') {
      $url = $host . $type . '?category=' . $quote_type . '&limit=' . $limit;
    }
    else {
      $url = $host . $type . '?limit=' . $limit;
    }
    $api_key = $config->get('api_key');
    try {
      $response = $client->request('GET', $url, [
        'headers' => [
          'X-Api-Key' => $api_key,
        ],
      ]);
      if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody(), TRUE);
        // Build the output.
        $output = [
          '#theme' => 'random_quotes_facts',
          '#data' => $data,
          '#cache' => [
            'max-age' => 0,
          ],
        ];
      }
      else {
        return [];
      }
    }
    catch (\Exception $e) {
      return [];
    }

    return $output;
  }

}
