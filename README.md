
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Features
 * Maintainers

INTRODUCTION
------------

The Random Quotes and Facts Generator module allows you to generate
Random quotes or facts and display them in a block on your website.
This module uses the API-Ninjas API to retrieve quotes and facts.

CONFIGURATION
-------------

To configure Random Quotes and Facts Generator:

1. Please signup first in the api-ninjas site at https://api-ninjas.com/register
   and then log into the portal
2. When logged into the portal, please create your API key at
   https://api-ninjas.com/profile

* Then  Navigate to the module configuration page:
1. Administration > Configuration > Development > Random quotes generator settings.
2. Set your API key for API-Ninjas.
3. Choose whether you want to display quotes or facts.
4. Choose how many (Limit) quotes or facts you want to display.

FEATURES
--------

The module strives to remain lightweight & efficient while integrating with
core Drupal features:

 * Blocks

The Random Quotes and Facts Generator module offers a variety of features to
enhance your website's user experience, including:

* Ability to generate random quotes or facts.
* Integration with the API-Ninjas API for retrieving quotes or facts.
* Easy configuration through the module configuration page.
* Option to choose between displaying quotes or facts.
* Option to set the number of quotes or facts to display.
* Block visibility settings for displaying the block on desired pages.
* Lightweight and efficient module.

MAINTAINERS
-----------

 * - Omkar Yewale - [omkar_yewale](https://www.drupal.org/u/omkar_yewale)
