(function ($) {

  Drupal.behaviors.initSlider = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $('.slider-nav').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: true,
          centerMode: true,
          focusOnSelect: true,
          centerPadding: '10px',
          responsive: [
            {
              breakpoint: 450,
              settings: {
                dots: false,
                slidesToShow: 1,
                centerPadding: '0px',
              }
            },
            {
              breakpoint: 420,
              settings: {
                autoplay: true,
                autoplaySpeed: 1000,
                dots: false,
                slidesToShow: 1,
                centerMode: false,
              }
            }
          ]
        });
      });

    }
  };

})(jQuery);